<?php

/**
 * @file
 * pMailer Module Admin Settings
 */

/**
 * implementation of hook_admin_settings
 * @return <type>
 */
function pmailer_admin_settings(){

  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'pmailer') . '/pmailer_api.php';

  $form['pmailer_account_info'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
    '#title' => t('Insert pMailer API Information'),
  );

  $form['pmailer_account_info']['pmailer_server'] = array(
    '#type' => 'textfield',
    '#title' => t('pMailer server'),
    '#required' => TRUE,
    '#default_value' => variable_get('pmailer_server', 'qa.pmailer.net'),
    '#description' => t('Specify pmailer server. '),
  );

  $form['pmailer_account_info']['pmailer_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('pMailer API key'),
    '#required' => TRUE,
    '#default_value' => variable_get('pmailer_api_key', ''),
    '#description' => t('The API key for your pmailer account. '),
  );

  $api_key = variable_get('pmailer_api_key', FALSE);
  $server = variable_get('pmailer_server', FALSE);

  $q = "";
  if ($api_key && $server){
    $q = new PMailerSubscriptionApiV1_0($server, $api_key);
  }
  else{
    form_set_error('', t('Please check your pMailer server and pMailer API-key settings '));
    return system_settings_form($form);
  }

  $form['account'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
    '#title' => t('Display Name & Surname field on subscription form.'),
  );

  $form['account']['save_name'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Name  & Surname fields'),    
    '#default_value' => variable_get('save_name', '0'),
    '#description' => t('Display and require the user to also provide a name and surname for the subscription.'),
  );

  $form['smart'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
    '#title' => t('Smart form setup'),
    '#tree' => TRUE,
  );

  $form['smart']['formactive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate smart form subscription'),
    '#default_value' => variable_get('formactive', '0'),
    '#description' => t('Please Note that the smart form will be deactivated once a user has submitted details.'),
  );

  $form['smart']['formcounter'] = array(
    '#type' => 'textfield',
    '#title' => t('Smart form page popup counter'),
    '#required' => TRUE,
    '#maxlength' => 2,
    '#size' => 2,
    '#default_value' => variable_get('formcounter', '12'),
    '#description' => t('Specify number of page refreshes before smart form appears.'),
  );

  //Only show lists if account API has been provided
  try{
    $getlists = $q->getLists();
  } catch (PMailerSubscriptionException $e){
    form_set_error('', t('Error: ' . $e->getMessage() . ' ( Please check your pMailer server and pMailer API-key settings )'));
  }

  $saved_lists = variable_get('pmailer_lists', array());

  if (empty($getlists) === false){
    /*
     * new lists display
     */
    $form['pmailer_lists']['lys'] = array(
      '#type' => 'fieldset',
      '#collapsed' => TRUE,
      '#title' => t('Select pMailer Subscription Lists (Users will only see the selected lists)'),
      '#tree' => TRUE,
    );

    foreach ($getlists['data'] as $list){

      if (!isset($saved_lists['list-' . $list['list_id']])) {
        $saved_lists['list-' . $list['list_id']] = 0;
      }
      if ($saved_lists['list-' . $list['list_id']] && isset($saved_lists)){
        $form['pmailer_lists']['lys']['list-' . $list['list_id']] = array(
          '#type' => 'checkbox',
          '#title' => t($list['list_name']),
          '#attributes' => array('checked' => 'checked'),
          '#default-value' => '0',
        );
      }
      else{
        $form['pmailer_lists']['lys']['list-' . $list['list_id']] = array(
          '#type' => 'checkbox',
          '#title' => t($list['list_name']),          
        );
      }
    }
  }
  else{   
    form_set_error('', t('You do not have any valid pMailer mailing lists.'));
  }

  $form['#validate'][] = 'pmailer_admin_settings_validate';
  return system_settings_form($form);
}

/**
 * validate the admin settings 
 *
 * @param <type> $form
 * @param <type> $form_state
 */
function pmailer_admin_settings_validate($form, &$form_state){
   $server = $form['pmailer_account_info']['pmailer_server']['#value'];
  if(substr($server, 0, 7) == 'http://' || substr($server, 0, 8) == 'https://'){
    form_set_error('title', t('URL is invalid. Please remove http:// in the beginning of your URL.'));
  }
  
  $lists = array();
  $is_checked = 0;
  if (isset($form_state['values']['lys'])){
    foreach ($form_state['values']['lys'] as $form_list => $selected) {
      $lists[$form_list] = $selected;

      if ($selected == 1) {
        $is_checked = 1;
      }
    }
  }

  if (isset($form_state['values']['smart']['formactive'])){
    variable_set('formactive', $form_state['values']['smart']['formactive']);
  }

  if (isset($form_state['values']['smart']['formcounter'])){
    variable_set('formcounter', $form_state['values']['smart']['formcounter']);
  }

  if (isset($form_state['values']['directory'])){
    variable_set('directory', $form_state['values']['directory']);
  }

  if (isset($form_state['values']['save_name'])){
    variable_set('save_name', $form_state['values']['save_name']);
  }

  variable_set('pmailer_server', $form_state['values']['pmailer_server']);
  variable_set('pmailer_api_key', $form_state['values']['pmailer_api_key']);

  if ($is_checked == 0){
    form_set_error('', t('Please select atleast one pMailer subscription'));
  }
  unset($form_state['values']['lys']);
  variable_set('pmailer_lists', $lists);
}

