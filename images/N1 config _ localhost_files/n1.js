(function (jQuery) {

    Drupal.behaviors.n1 = {
        attach: function(context) {

            var smart_popup_counter = 9999;
            var smartform_active = Drupal.settings.js_vars.smart_form_active;
            var smartform_counter = Drupal.settings.js_vars.smart_form_counter;
            // alert('active '+ smartform_active + ' ' + smartform_counter );

  eraseCookie('AlreadySubmitted');
            if (parseInt(smartform_active) == 1 && (readCookie('AlreadySubmitted') == null))    
            {
                smart_popup_counter = parseInt(smartform_counter);
            }         
            
            var reload = function(data)
            {     
                jQuery('#arc').html(data)
            }
                
            var start = 1;
            var end = 11;
            var total = 20;
            var calc_last = 0;
            
            if (parseInt(jQuery('#pager_total').val()) <= 5)
            {
                jQuery('#page_forward').attr('disabled','disabled');
            }
           
            
            jQuery('#page_forward').bind('click', function(){
                
                jQuery('#page_back').attr('disabled','');     
              
                current_Page = parseInt(jQuery('#pager_first').val()) + 1;
                jQuery('#page_info').html(' Page '+current_Page+' / ' +jQuery('#pager_last').val() + ' ');
                
                //if on last page disable forward button
                if (parseInt(jQuery('#pager_first').val()) == parseInt(jQuery('#pager_last').val()) -1 )
                {
                    jQuery('#page_forward').attr('disabled','disabled');                    
                } else
{
                    jQuery('#page_forward').attr('disabled','');     
                }
               
                jQuery('#arc').html("<div align='center'><img src='"+Drupal.settings.basePath+"/sites/all/modules/n1/images/ajax-loader.gif' /></div>");
                jQuery.get('load/'+start,  null, reload);
                
                // preventing entire page from reloading
                false;
            });
            
            jQuery('#page_back').bind('click', function(){
              
                current_Page = parseInt(jQuery('#pager_first').val()) - 1;
                jQuery('#page_info').html(' Page '+current_Page+' / ' +jQuery('#pager_last').val() + ' ');
              
                // Disabled back button when pager is at 1
                if (parseInt(jQuery('#pager_first').val()) == 2)
                {
                    jQuery('#page_back').attr('disabled','disabled');                    
                } else
{
                    jQuery('#page_back').attr('disabled','');     
                }
                
              
                //if on last page disable forward button
                if (parseInt(jQuery('#pager_first').val()) == parseInt(jQuery('#pager_last').val()))
                {
                    jQuery('#page_forward').attr('disabled','');                    
                } 
            
               
                start = parseInt(jQuery('#pager_first').val()) - 1;
                          
                jQuery('#arc').html("<div align='center'><img src='"+Drupal.settings.basePath+"/sites/all/modules/n1/images/ajax-loader.gif' /></div>");
                jQuery.get('load/'+start, null, reload);
                // preventing entire page from reloading
                false;
            });
           

            jQuery('#signupbutton').bind('click', function(){
      
                var lists = '';
                var counter_check = 0;
                jQuery('.listsSelected ').each(function() {          
                    if (jQuery(this).attr('checked'))
                    {                
                        lists += jQuery(this).val() + ',';         
                        counter_check = counter_check + 1;
                    }
                }); 
       
                /*
                 * Javascript Validation
                 */
                if(jQuery('#popup_name').val() == '')
                {
                    jQuery('#validate_name').html('<p>Please enter a Name.</p>');
                } else
{
                    jQuery('#validate_name').html('');
                }
                if(jQuery('#popup_surname').val() == '')
                {
                    jQuery('#validate_surname').html('<p>Please enter a Surname.</p>');
                }
                else
                {
                    jQuery('#validate_surname').html('');
                }
                if(jQuery('#popup_email').val() == '')
                {
                    jQuery('#validate_email').html('<p>Please enter an Email address.</p>');
                }
                else if(isEmail(jQuery('#popup_email').val()) == false)
                {
                    jQuery('#validate_email').html('<p>Invalid email address. Please re-enter.</p>');
                } else
{       
                    jQuery('#validate_email').html('');
                }
           
                if (counter_check == 0)
                {
                    jQuery('#validate_lists').html('<p>Please select atleast one list.</p>');               
                } else
{
                    jQuery('#validate_lists').html(''); 
                }
       
       
                /*
                 *  if all validation pass
                 *  
                 */
       
                var frmDrupal2 = function(data)
                {     
                    jQuery('#qwer').html(data)
                    setTimeout("jQuery('#qwer').dialog('close')",5000);
                   
                    createCookie('AlreadySubmitted','1',365);
                               
                }
                
                if(jQuery('#popup_name').attr('value') != '' && jQuery('#popup_surname').val() != '' && jQuery('#popup_email').val() != '' && counter_check != 0 && isEmail(jQuery('#popup_email').val()))
                {                    
                    jQuery.get('doneit2/'+ jQuery('#popup_name').val()  + '/' + jQuery('#popup_surname').val() + '/' + jQuery('#popup_email').val() +'/' + lists, null, frmDrupal2);
                    jQuery('#qwer').html('Submitting you subscription details.<br/><div align="center"><img src="'+Drupal.settings.basePath+'/sites/all/modules/n1/images/ajax-loader.gif" /></div>');                    
                    // preventing entire page from reloading
                    return false;
                }
       
       
            });
    
            jQuery('#cancelbutton').bind('click', function(){           
                // alert('cancel button clicked ' + jQuery('#popup_name').val() + ' ' + jQuery('#popup_surname').val() + ' ' + jQuery('#popup_email').val() + ' ___ ' + lists);
        
                //Hide the smart popup and don;t show it again
                          
                createCookie('AlreadySubmitted','1',365);
                jQuery( "#qwer" ).dialog({
                    hide: 'slide'
                });
                jQuery("#qwer").dialog("close");
            });
    
            if (readCookie('jannie') == null)
            {
                createCookie('jannie','0',365);
            }
            else
            {              
                if (parseInt(readCookie('jannie')) >= parseInt(smart_popup_counter))
                {
                    if (parseInt(smartform_active) == 1)    
                    {
                        jQuery('#qwer').dialog({
                            modal:true,
                            title: 'Subscriptions',
                            minHeight: 455
                        });
                    }
                    //var $tmp = jQuery('#user-subscribe-form').html();
                    // var $ff = jQuery('http://localhost/drupal70/theming_ex')
                        
                    //jQuery('#dial').css('display','block');
                    eraseCookie('jannie');
                    createCookie('jannie','0',365);
                }
                else
                {
                    doCookie('jannie');
                }
            }
   
        //jQuery("<div id='dial' style='border:1px solid #999; padding:2px; margin:2px; position:absolute; top:10px; left:10px; background:#eee;'>" + jQuery('#block-system-main').html() + "</div>").insertAfter('#main');
        }
    };
    
    
            
    
}(jQuery));


function isEmail(str){

    var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    if (filter.test(str))
        return true
    else{
        return false
    }
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}
      
// document.cookie = 'jannie=0; path=/';  
      


function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}


function eraseCookie(name) {
    createCookie(name,"",-1);
}
      
function doCookie(str)
{
    var st = readCookie(str);
        
    if (st)
    {            
        // document.getElementById('showCookie').innerHTML = st;       
        var addtoCookie = parseInt(st) + 1;
        document.cookie = 'jannie='+addtoCookie+'; path=/';            
                        
    }else
    {
        alert('COOKIE NAME INVALID');
    }
         
}
