(function (jQuery) {

    Drupal.behaviors.pmailer = {
        attach: function(context) {
   
            // Custom variables send from the modules admin setup      
            var smartform_active = Drupal.settings.js_vars.smart_form_active;
            var smartform_counter = Drupal.settings.js_vars.smart_form_counter;
        
            if (getCookie("smartform") == null){
                setCookie('smartform','0',60);
            }
            else{
                if (parseInt(getCookie('smartform')) >= parseInt(smartform_counter)){ 
                    // Show the smart form only if it has been selected in the ADMIN area                     
                    if ((parseInt(smartform_active) == 1) && (getCookie('AlreadySubmitted') == null || parseInt(getCookie('AlreadySubmitted')) != 1 )){
                        jQuery('#qwer').dialog({
                            modal:true,
                            title: 'Subscriptions',
                            minHeight: 435
                        });
                    }
                    setCookie('smartform','0',60);
                }                
                setCookie('smartform', (parseInt(getCookie('smartform'))+1),60 );
            }
                
             
            jQuery('#signupbutton').bind('click', function(){
      
                var lists = '';
                var list_counter_check = 0;
                jQuery('.listsSelected ').each(function() {          
                    if (jQuery(this).attr('checked')){                
                        lists += jQuery(this).val() + ',';         
                        list_counter_check = list_counter_check + 1;
                    }
                }); 
       
                /*
                     * Javascript Validation
                     */
                if(jQuery('#popup_name').val() == ''){
                    jQuery('#validate_name').html('<p>Please enter a Name.</p>');
                } else{
                    jQuery('#validate_name').html('');
                }
                if(jQuery('#popup_surname').val() == ''){
                    jQuery('#validate_surname').html('<p>Please enter a Surname.</p>');
                }
                else{
                    jQuery('#validate_surname').html('');
                }
                if(jQuery('#popup_email').val() == ''){
                    jQuery('#validate_email').html('<p>Please enter an Email address.</p>');
                }
                else if(isEmail(jQuery('#popup_email').val()) == false){
                    jQuery('#validate_email').html('<p>Invalid email address. Please re-enter.</p>');
                } else{       
                    jQuery('#validate_email').html('');
                }
           
                if (list_counter_check == 0){
                    jQuery('#validate_lists').html('<p>Please select atleast one list.</p>');               
                } else{
                    jQuery('#validate_lists').html(''); 
                }
       
       
                // If all validation pass
                if((jQuery('#popup_name').attr('value') != '') && (jQuery('#popup_surname').val() != '') && (jQuery('#popup_email').val() != '') && (list_counter_check != 0) && (isEmail(jQuery('#popup_email').val()))){  
                    jQuery.get('smartform/'+ jQuery('#popup_name').val()  + '/' + jQuery('#popup_surname').val() + '/' + jQuery('#popup_email').val() +'/' + lists, null, frmDrupal2);
                        
                    jQuery('#qwer').html('Submitting you subscription details.<br/><div align="center"><img src="'+Drupal.settings.basePath+'/sites/all/modules/pmailer/images/ajax-loader.gif" /></div>');                    
                    // preventing entire page from reloading
                    return false;
                }
       
       
            });
            
            var frmDrupal2 = function(data){    
                   
                jQuery('#qwer').html(data)
                setTimeout("jQuery('#qwer').dialog('close')",2000);
                   
                setCookie('AlreadySubmitted','1',60);
                               
            }
            
    
            jQuery('#cancelbutton').bind('click', function(){           
                //Hide the smart popup and don;t show it again
                  
                setCookie('AlreadySubmitted','1',60);
                jQuery( "#qwer" ).dialog({
                    hide: 'slide'
                });
                jQuery("#qwer").dialog("close");
            });
    
            
        }
    
    
    }
    
}(jQuery));


function isEmail(str){

    var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    if (filter.test(str))
        return true
    else{
        return false
    }
}


function getCookie(c_name){
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++){
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name){
            return unescape(y);
        }
    }
}

function setCookie(c_name,value,exdays){
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}